package pragma.roosvell.personmicroservice.infrastructure.error;


    public class NotFoundExc extends RuntimeException {
        public NotFoundExc(String message) {
            super(message);
        }

    }

