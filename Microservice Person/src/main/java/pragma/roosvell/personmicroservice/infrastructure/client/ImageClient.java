package pragma.roosvell.personmicroservice.infrastructure.client;


import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pragma.roosvell.personmicroservice.dominio.image.model.Image;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;


import java.util.ArrayList;
import java.util.List;

@FeignClient(name="image-service")
public interface ImageClient {
    public static String none = "none";
    public static final String IMAGE_SERVICE = "imageService";

    @GetMapping("/api/images/{id}")
    @CircuitBreaker(name=IMAGE_SERVICE, fallbackMethod = "getImagesFallback")
    public ResponseEntity<List<Image>> getImagesByPersonId(@PathVariable("id") Long id);


      @DeleteMapping("/api/image/{id}")
      @CircuitBreaker(name=IMAGE_SERVICE, fallbackMethod = "deleteImageFallback")
      public ResponseEntity<String>deleteImageById(@PathVariable("id") String id);


    @PutMapping("/api/images")
    @CircuitBreaker(name=IMAGE_SERVICE, fallbackMethod = "updatePersonFallback")
    public ResponseEntity<String> updatePerson(@RequestParam String idImage, @RequestParam Long idPerson);

    default ResponseEntity<List<Image>> getImagesFallback (Long id, Exception e){
        List<Image> listImages = new ArrayList<>();
        Person emptyPerson = Person.builder()
                .id(0L)
                .dni(0L)
                .name(none)
                .lastname(none).build();
        Image emptyImage = Image.builder()
                .idImage(none)
                .person(emptyPerson)
                .file(null).build();
        listImages.add(emptyImage);
        return ResponseEntity.ok(listImages);
    }
    default ResponseEntity<String> deleteImageFallback(String id, Exception e) {
        return ResponseEntity.ok(none);
    }

    default ResponseEntity<String> updatePersonFallback(String idImage, Long idPerson, Exception e) {
        return ResponseEntity.ok(none);
    }

}
