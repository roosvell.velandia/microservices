package pragma.roosvell.personmicroservice.infrastructure.person.persondao;

import org.springframework.data.jpa.repository.JpaRepository;
import pragma.roosvell.personmicroservice.infrastructure.person.entities.PersonEntity;

public interface PersonDao extends JpaRepository<PersonEntity,Long> {
}
