package pragma.roosvell.personmicroservice.infrastructure.person.adapter;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pragma.roosvell.personmicroservice.application.mapper.PersonMapper;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.image.model.Image;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.client.ImageClient;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;
import pragma.roosvell.personmicroservice.infrastructure.person.persondao.PersonDao;
import pragma.roosvell.personmicroservice.infrastructure.person.entities.PersonEntity;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Repository

public class PersonAdapter implements PersonRepository {
    private static String personNf = "Person Not Found ";
    private final PersonDao persondao;
    private final PersonMapper personMapper;
    private final ImageClient imagedao;
    public Person createPerson(Person person) {
        return personMapper.entityToPerson(persondao.save(personMapper.personToEntity(person)));
    }

    @Override
    public Person updatePerson(Person person, Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            PersonEntity personaUpdate = personDB.get();
            personaUpdate.setId(id);
            personaUpdate.setLastname(person.getLastname());
            personaUpdate.setName(person.getName());
            personaUpdate.setDni(person.getDni());
            persondao.save(personaUpdate);
            ResponseEntity<List<Image>> personImages = imagedao.getImagesByPersonId(id);
            for(Image personImage : personImages.getBody()){
                imagedao.updatePerson(personImage.get_id(),id);
            }
            return personMapper.entityToPerson(personaUpdate);
        } else {
            throw new NotFoundExc(personNf + id);
        }
    }

    @Override
    public List<Person> getPeople() {
        return personMapper.listEntityToListPerson(persondao.findAll());
    }

    @Override
    public Person getPersonById(Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            return personMapper.entityToPerson(personDB.get());
        } else
        {
            throw new NotFoundExc(personNf + id );
        }

    }

    @Override
    public String deletePerson(Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            ResponseEntity<List<Image>> personImages = imagedao.getImagesByPersonId(id);
            for(Image personImage : personImages.getBody()){
                imagedao.deleteImageById(personImage.get_id());
            }
            persondao.deleteById(id);
            return "Person deleted";
        } else
        {
            throw new NotFoundExc(personNf + id);
        }

    }
}
