package pragma.roosvell.personmicroservice.infrastructure.person.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pragma.roosvell.personmicroservice.application.dto.PersonDTO;
import pragma.roosvell.personmicroservice.application.mapper.PersonMapper;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor
public class ControllerUpdatePerson {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonMapper personMapper;

    @ApiOperation(value="API to Update people in MySQL database",response=Person.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="New Person Details Retrieved",response=Person.class),
            @ApiResponse(code=201,message="Person updated"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @PutMapping
    public ResponseEntity<Person> updatePerson(@RequestBody PersonDTO personaDTO, @RequestParam Long id) throws NotFoundExc {
        return ResponseEntity.ok().body(personRepository.updatePerson(personMapper.personDtoToPerson(personaDTO),id));
    }

}
