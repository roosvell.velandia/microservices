package pragma.roosvell.personmicroservice.infrastructure.person.controller;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

import java.io.IOException;

@RestController
@RequestMapping(value = "/api/person")
@RequiredArgsConstructor
public class ControllerGetPersonById {

    @Autowired
    PersonRepository personRepository;

    @ApiOperation(value="API to Get people by id in MySQL database",response=Person.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Person Details Retrieved",response=Person.class),
            @ApiResponse(code=201,message="Query well executed"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping("{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id) throws NotFoundExc {

        System.out.println("in controller");
            return ResponseEntity.ok().body(personRepository.getPersonById(id));
    }
}