package pragma.roosvell.personmicroservice.application.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pragma.roosvell.personmicroservice.application.dto.PersonDTO;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.person.entities.PersonEntity;
import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonMapper {
    PersonEntity personToEntity (Person person);
    Person entityToPerson (PersonEntity personEntity);
    Person personDtoToPerson (PersonDTO personDTO);
    List<Person> listEntityToListPerson (List<PersonEntity> listEntity);
    List<PersonEntity> listPersonToListEntity (List<Person> listPerson);
}
