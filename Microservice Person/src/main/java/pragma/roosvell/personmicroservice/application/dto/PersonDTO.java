package pragma.roosvell.personmicroservice.application.dto;

import lombok.*;

@Data
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class PersonDTO {
    private Long dni;
    private String name;
    private String lastname;
}
