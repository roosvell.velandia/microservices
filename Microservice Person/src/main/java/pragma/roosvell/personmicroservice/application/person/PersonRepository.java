package pragma.roosvell.personmicroservice.application.person;

import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

import java.util.List;

public interface PersonRepository {
    Person createPerson (Person person);
    Person updatePerson (Person person, Long id) throws NotFoundExc;
    List<Person> getPeople ();
    Person getPersonById (Long id) throws NotFoundExc;
    String deletePerson (Long id) throws NotFoundExc;
}
