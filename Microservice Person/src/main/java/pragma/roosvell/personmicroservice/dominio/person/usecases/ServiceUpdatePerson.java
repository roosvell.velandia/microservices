package pragma.roosvell.personmicroservice.dominio.person.usecases;

import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

public class ServiceUpdatePerson {
    private final PersonRepository personRepository;

    public ServiceUpdatePerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person updatePerson (Person person, Long id) throws NotFoundExc {
        if(personRepository.getPersonById(id) == null){
            throw new NotFoundExc("person " + id + "Not Found");
        }
        return this.personRepository.updatePerson(person, id);
    }
}
