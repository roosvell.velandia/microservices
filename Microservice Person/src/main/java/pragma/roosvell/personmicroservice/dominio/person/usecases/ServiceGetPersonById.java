package pragma.roosvell.personmicroservice.dominio.person.usecases;

import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

public class ServiceGetPersonById {
    private final PersonRepository personRepository;

    public ServiceGetPersonById(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person getPersonById (Long id) throws NotFoundExc{
        if (this.personRepository.getPersonById(id) == null){
            throw new NotFoundExc("Person " + id + " Not found.");
        }else{
            return this.personRepository.getPersonById(id);
        }

    }
}
