package pragma.roosvell.personmicroservice.dominio.person.usecases;

import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;

import java.util.List;

public class ServiceGetPeople {
    private final PersonRepository personRepository;

    public ServiceGetPeople(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getPeople (){
        return this.personRepository.getPeople();
    }
}
