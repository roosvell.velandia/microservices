package pragma.roosvell.personmicroservice.dominio.person.usecases;

import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;

public class ServiceCreatePerson {
    private final PersonRepository personRepository;

    public ServiceCreatePerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person createPerson (Person person){
        return this.personRepository.createPerson(person);
    }
}
