package pragma.roosvell.domain.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.usecases.ServiceCreatePerson;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;


class ServiceCreatePersonTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceCreatePerson service = new ServiceCreatePerson(repository);

    @Test
    void createPerson() {
        repository.createPerson(DataPerson.createPerson1().get());
        assertDoesNotThrow( ()->{
            service.createPerson(DataPerson.createPerson1().get());
        });
    }
}