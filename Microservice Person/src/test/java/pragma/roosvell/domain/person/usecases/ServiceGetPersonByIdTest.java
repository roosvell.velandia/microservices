package pragma.roosvell.domain.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.dominio.person.usecases.ServiceGetPersonById;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;


import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ServiceGetPersonByIdTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceGetPersonById service = new ServiceGetPersonById(repository);

    @Test
    void getPersonById() {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        when(repository.getPersonById(2L)).thenReturn(DataPerson.createPerson2().get());

        Person person1 = service.getPersonById(1L);
        Person person2 = service.getPersonById(2L);
        //PersonasDTOResponse persona3 = servicio.getPersonaById(3L);


        assertSame(1L,person1.getId());
        assertSame(2L,person2.getId());

        verify(repository, times(2)).getPersonById(1L);
        verify(repository, times(2)).getPersonById(2L);
        assertThrows(NotFoundExc.class, ()->{
            service.getPersonById(3L);
        });
        verify(repository, times(5)).getPersonById(any(Long.class));
    }
}