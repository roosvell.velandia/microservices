package pragma.roosvell.domain.person.usecases;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.usecases.ServiceDeletePerson;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = ServiceDeletePerson.class)
class ServiceDeletePersonTest {
    @MockBean
    PersonRepository repository;

    @Autowired
    ServiceDeletePerson service;
    @Test
    void deletePeople() throws Exception {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        System.out.println(service.deletePerson(1L));
        assertThrows(NotFoundExc.class, ()->{
            System.out.println(service.deletePerson(3L));
        });
        verify(repository, times(1)).deletePerson(1L);
        verify(repository).deletePerson(any(Long.class));
    }

}