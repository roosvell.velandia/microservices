package pragma.roosvell.domain.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;
import pragma.roosvell.personmicroservice.dominio.person.usecases.ServiceUpdatePerson;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServiceUpdatePersonTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceUpdatePerson service = new ServiceUpdatePerson(repository);

    @Test
    void updatePerson() throws Exception {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        when(repository.updatePerson(DataPerson.createPerson1().get(),1L))
                .thenReturn(DataPerson.createPerson1().get());
        assertDoesNotThrow( ()->{
            service.updatePerson(DataPerson.createPerson1().get(), 1L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updatePerson(DataPerson.createPerson1().get(),3L);
        });
    }
}