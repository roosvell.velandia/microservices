package pragma.roosvell.personmicroservice.infrastructure.person.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerGetPersonById.class)
class ControllerGetPersonByIdTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @Test
    void getPersonById() throws Exception {
        when(service.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
       mvc.perform(get("/api/person/1").contentType(MediaType.APPLICATION_JSON)
                       .queryParam("id", String.valueOf(1L)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.dni").value("12345"))
               .andDo(print());

    }
}