package pragma.roosvell.personmicroservice.infrastructure.person.adapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.reactive.server.WebTestClient;
import pragma.roosvell.data.DataImage;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.mapper.PersonMapper;
import pragma.roosvell.personmicroservice.dominio.image.model.Image;
import pragma.roosvell.personmicroservice.dominio.person.model.Person;
import pragma.roosvell.personmicroservice.infrastructure.client.ImageClient;
import pragma.roosvell.personmicroservice.infrastructure.error.NotFoundExc;
import pragma.roosvell.personmicroservice.infrastructure.person.entities.PersonEntity;
import pragma.roosvell.personmicroservice.infrastructure.person.persondao.PersonDao;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonAdapterTest {
    @MockBean
    PersonDao repository;

    @MockBean
    ImageClient imageRepository;

    @Autowired
    PersonMapper mapper;

    @Autowired
    PersonAdapter service;

    @Autowired
    private TestRestTemplate client;

    @Test
    void createPerson() {
        when(repository.save(mapper.personToEntity(DataPerson.createPerson1().get()))).thenReturn(mapper.personToEntity(DataPerson.createPerson1().get()));
        Person person1 = service.createPerson(DataPerson.createPerson1().get());
        assertEquals(person1.getDni(),DataPerson.createPerson1().get().getDni());
        assertEquals(person1.getName(),DataPerson.createPerson1().get().getName());
        assertEquals(person1.getLastname(),DataPerson.createPerson1().get().getLastname());
    }

    @Test
    void updatePerson() {
        when(imageRepository.getImagesByPersonId(1L)).thenReturn(DataImage.createImages());
        when(repository.findById(1L))
                .thenReturn(Optional.ofNullable(mapper
                        .personToEntity(DataPerson.createPerson1().get())));
        when(repository.save(mapper.personToEntity(DataPerson.createPerson1().get())))
                .thenReturn(mapper.personToEntity(DataPerson.createPerson1().get()));
        //when(client.getForEntity("http://localhost:8080/api/image",Image[].class))
         //       .thenReturn((ResponseEntity<Image[]>) DataImage.createImages());

        Person person1 = service.updatePerson(DataPerson.createPerson1().get(),1L);
        assertEquals(person1.getName(),DataPerson.createPerson1().get().getName());
        assertEquals(person1.getLastname(),DataPerson.createPerson1().get().getLastname());
        assertEquals(person1.getDni(),DataPerson.createPerson1().get().getDni());
        assertThrows(RuntimeException.class, ()->{
            service.updatePerson(DataPerson.createPerson3().get(),3L);
        });
    }

    @Test
    void getPeople() {
        when(repository.findAll()).thenReturn(mapper.listPersonToListEntity(DataPerson.createPeople()));
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        when(repository.findById(2L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson2().get())));
        List<Person> people = service.getPeople();
        System.out.println(people);
        for (Person person:people){
            service.getPersonById(person.getId());
            assertNotNull(person);
        }
    }

    @Test
    void getPersonById() {
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        when(repository.findById(2L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson2().get())));

        Person persona1 = service.getPersonById(1L);
        Person persona2 = service.getPersonById(2L);
        //PersonasDTOResponse persona3 = servicio.getPersonaById(3L);


        assertSame(1L,persona1.getId());
        assertSame(2L,persona2.getId());

        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).findById(2L);
        assertThrows(NotFoundExc.class, ()->{
            service.getPersonById(3L);
        });
        verify(repository, times(3)).findById(any(Long.class));

    }

    @Test
    void deletePerson() {
        ResponseEntity<String> responseEntity = new ResponseEntity<String>("image deleted", HttpStatus.OK);
        when(imageRepository.deleteImageById("id1")).thenReturn(responseEntity);
        when(imageRepository.deleteImageById("id2")).thenReturn(responseEntity);
        when(imageRepository.getImagesByPersonId(1L)).thenReturn(DataImage.createImages());
        when(repository.findById(1L))
                .thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        repository.delete(mapper.personToEntity(DataPerson.createPerson1().get()));
        System.out.println(service.deletePerson(1L));
        assertThrows(NotFoundExc.class, ()->{
            service.deletePerson(3L);
        });
        verify(repository).delete(any(PersonEntity.class));

        when(imageRepository.getImagesByPersonId(1L))
                .thenReturn((ResponseEntity<List<Image>>) DataImage.createImages());
        ResponseEntity<List<Image>> images = imageRepository.getImagesByPersonId(1L);
        assertNotNull(images);
        for(Image image:images.getBody()){
            assertEquals("image deleted", imageRepository.deleteImageById(image.get_id()).getBody());
        }
    }

}