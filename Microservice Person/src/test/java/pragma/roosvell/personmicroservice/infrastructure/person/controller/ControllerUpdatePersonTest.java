package pragma.roosvell.personmicroservice.infrastructure.person.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.data.DataPerson;
import pragma.roosvell.personmicroservice.application.mapper.PersonMapper;
import pragma.roosvell.personmicroservice.application.person.PersonRepository;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(ControllerUpdatePerson.class)
class ControllerUpdatePersonTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @MockBean
    PersonMapper personMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void updatePerson() throws Exception {
        Long id = 1L;
        when(service.updatePerson(any(),anyLong()))
                .thenReturn(DataPerson.createPerson1().get());
        mvc.perform(put("/api/person").contentType(MediaType.APPLICATION_JSON)
                .queryParam("id", objectMapper.writeValueAsString(id))
                        .content(objectMapper.writeValueAsString(DataPerson.createPerson1().get())))
                        .andDo(print());
    }
}