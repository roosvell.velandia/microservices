package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;

import java.util.List;

public class ServiceGetImages {
    private final ImageRepository imageRepository;

    public ServiceGetImages(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public List<Image> getImages () {
        return this.imageRepository.getImages();
    }
}
