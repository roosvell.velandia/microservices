package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;

import java.util.List;

public class ServiceGetImagesByPersonId {
    private final ImageRepository imageRepository;
    private final PersonClient personRepository;

    public ServiceGetImagesByPersonId(ImageRepository imageRepository, PersonClient personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }
    public List<Image> getImagesByPersonId (Long id) throws NotFoundExc {
     if(personRepository.getPersonById(id)==null){
       throw new NotFoundExc("Person " + id + " Not Found");
     } else {
         return this.imageRepository.getImagesByPersonId(id);
     }

    }
}
