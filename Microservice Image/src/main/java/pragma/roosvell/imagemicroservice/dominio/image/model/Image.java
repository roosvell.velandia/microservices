package pragma.roosvell.imagemicroservice.dominio.image.model;

import pragma.roosvell.imagemicroservice.dominio.person.model.Person;

public class Image {
    private String idImage;
    private byte[] file;
    private Person person;

    public String get_id() {
        return idImage;
    }

    public void set_id(String idImage) {
        this.idImage = idImage;
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Image() {
    }

    public Image(String idImage, byte[] file, Person person) {
        this.idImage = idImage;
        this.file = file;
        this.person = person;
    }
}
