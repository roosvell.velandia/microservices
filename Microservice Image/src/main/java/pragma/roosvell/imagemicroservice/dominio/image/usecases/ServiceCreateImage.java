package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;

import java.io.IOException;

public class ServiceCreateImage {
    private final ImageRepository imageRepository;
    private final PersonClient personRepository;

    public ServiceCreateImage(ImageRepository imageRepository, PersonClient personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }

    public Image createImage (MultipartFile file, Long idPerson) throws IOException, NotFoundExc {
        if(personRepository.getPersonById(idPerson) == null){
            throw new NotFoundExc("person " + idPerson + " Not Found");
        }
        return this.imageRepository.createImage(file, idPerson);
    }
}
