package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;

import java.io.IOException;

public class ServiceUpdateImage {
    private final ImageRepository imageRepository;
    private final PersonClient personRepository;

    public ServiceUpdateImage(ImageRepository imageRepository, PersonClient personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }

    public Image updateImage (MultipartFile file,String _id, Long idPerson) throws IOException, NotFoundExc {
        if (imageRepository.getImageById(_id) == null){
            throw new NotFoundExc("Image " + _id + " Not Found");
        } else if(personRepository.getPersonById(idPerson) == null) {
            throw new NotFoundExc("Person " + idPerson + " Not Found");
        }else{
            return this.imageRepository.updateImage(file,_id,idPerson);
        }
    }
}
