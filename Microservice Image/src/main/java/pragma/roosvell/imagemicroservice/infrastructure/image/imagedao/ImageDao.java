package pragma.roosvell.imagemicroservice.infrastructure.image.imagedao;

import org.springframework.data.mongodb.repository.MongoRepository;
import pragma.roosvell.imagemicroservice.infrastructure.image.entities.ImageEntity;

import java.util.List;

public interface ImageDao extends MongoRepository<ImageEntity,String> {
    List<ImageEntity> findByPersonId(Long id);
}
