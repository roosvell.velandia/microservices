package pragma.roosvell.imagemicroservice.infrastructure.client;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;

import java.util.ArrayList;
import java.util.List;

@FeignClient(name="person-service")
public interface PersonClient {
    public static String none = "none";
    public static final String PERSON_SERVICE = "personService";

    @GetMapping("/api/person/{id}")
    @CircuitBreaker(name = PERSON_SERVICE, fallbackMethod = "getPersonFallback")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id);

    default ResponseEntity<Person> getPersonFallback (Long id, Exception e){
        Person emptyPerson = Person.builder()
                .id(0L)
                .dni(0L)
                .name(none)
                .lastname(none).build();
        return ResponseEntity.ok(emptyPerson);
    }
}
