package pragma.roosvell.imagemicroservice.infrastructure.image.entities;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;


@Setter
@Getter
@Data
@RequiredArgsConstructor
@Document(collection = "ImagesMongodb")
public class ImageEntity {

    private String _id;
    private byte[] file;
    private Person person;
}
