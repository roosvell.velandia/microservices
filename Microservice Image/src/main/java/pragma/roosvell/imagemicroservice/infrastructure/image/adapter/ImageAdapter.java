package pragma.roosvell.imagemicroservice.infrastructure.image.adapter;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.application.mapper.ImageMapper;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;
import pragma.roosvell.imagemicroservice.infrastructure.image.entities.ImageEntity;
import pragma.roosvell.imagemicroservice.infrastructure.image.imagedao.ImageDao;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;


@Repository
@RequiredArgsConstructor
public class ImageAdapter implements ImageRepository {
    private static String personNf = "Person Not Found ";
    private static String registerNf = "Register Not Found ";
    private final ImageDao imageDao;
    private final ImageMapper imageMapper;
    private final PersonClient personDao;

    @Override
    public Image createImage(MultipartFile imageFile, Long idPerson) throws IOException, NotFoundExc {
        ImageEntity image = new ImageEntity();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imageFile.getBytes();
        image.setFile(fileContent);
        System.out.println("in image controller 1");
        ResponseEntity<Person> persondb = personDao.getPersonById(idPerson);
        if (persondb!=null) {
            // actualizo los datos en la imagen
            System.out.println("in image controller 2");
            image.setPerson(persondb.getBody());

        } else {
            System.out.println("in image controller 3");
            throw new NotFoundExc(personNf + idPerson);
        }
        imageDao.save(image);
        return imageMapper.imageEntityToImage(image);
    }

    @Override
    public Image updateImage(MultipartFile imageFile, String id, Long idPerson) throws IOException, NotFoundExc {
        Optional <ImageEntity> imagedb= this.imageDao.findById(id);
        ResponseEntity<Person> persondb = this.personDao.getPersonById(idPerson);
        // busco si existe la persona
        if (persondb!=null) {
            // actualizo los datos en la imagen
            if (imagedb.isPresent()) {
                ImageEntity imageUpdate = imagedb.get();
                imageUpdate.setPerson(persondb.getBody());
                byte[] fileContent = imageFile.getBytes();
                imageUpdate.setFile(fileContent);
                // devuelvo la imagen actualizada.
                imageDao.save(imageUpdate);
                return imageMapper.imageEntityToImage(imageUpdate);
            } else {
                throw new NotFoundExc(registerNf + id);
            }
        } else {
            throw new NotFoundExc(personNf + idPerson);
        }
        //convierto la imagen en un mapa de bits y lo transformo en base 64

    }

    @Override
    public List<Image> getImages() {
        return imageMapper.listImageEntityToListImage(imageDao.findAll());
    }

    @Override
    public Image getImageById(String id) throws NotFoundExc {
        Optional<ImageEntity> imagedb = this.imageDao.findById(id);
        if(((Optional<?>) imagedb).isPresent()){
            return imageMapper.imageEntityToImage(imagedb.get());
        } else {
            throw new NotFoundExc(registerNf + id);
        }
    }

    @Override
    public String deleteImage(String id) throws NotFoundExc {
        Optional<ImageEntity> imagedb = this.imageDao.findById(id);
        if(((Optional<?>) imagedb).isPresent()){
            imageDao.delete(imagedb.get());
            return "image deleted";
        } else {
            throw new NotFoundExc(registerNf + id);
        }
    }

    @Override
    public List<Image> getImagesByPersonId(Long id) {
        ResponseEntity<Person> persondb = this.personDao.getPersonById(id);
        if (persondb!=null) {
            return imageMapper.listImageEntityToListImage(imageDao.findByPersonId(id));
        } else {
            throw new NotFoundExc(personNf + id);
        }
    }

    @Override
    public String updatePerson(String _id, Long idPerson) throws NotFoundExc{

        ResponseEntity<Person> persondb = this.personDao.getPersonById(idPerson);
        // busco si existe la persona
        if (persondb!=null) {
            Optional <ImageEntity> imagedb= this.imageDao.findById(_id);
            if (imagedb.isPresent()) {
                ImageEntity imageUpdate = imagedb.get();
                // actualizo los datos en la imagen
                imageUpdate.setPerson(persondb.getBody());
                // devuelvo la imagen actualizada.
                imageMapper.imageEntityToImage(imageDao.save(imageUpdate));
                return "image updated";
            } else {
                throw new NotFoundExc(registerNf + _id);
            }

        } else {
            throw new NotFoundExc(personNf + idPerson);
        }

    }
}
