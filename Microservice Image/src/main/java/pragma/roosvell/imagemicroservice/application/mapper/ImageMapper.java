package pragma.roosvell.imagemicroservice.application.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.image.entities.ImageEntity;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ImageMapper {
    Image imageEntityToImage(ImageEntity imageEntity);
    List<Image> listImageEntityToListImage(List<ImageEntity> listImages);
    ImageEntity imagetoImageEntity (Image image);
    List<ImageEntity> listImageToListImageEntity(List<Image> listImages);
}
