package pragma.roosvell.imagemicroservice.application.dto;

import lombok.*;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;

@Data
@Setter
@Getter
@RequiredArgsConstructor
public class ImageDTO {
    private String idImage;
    private String imageBase64;
    private Person person;
}
