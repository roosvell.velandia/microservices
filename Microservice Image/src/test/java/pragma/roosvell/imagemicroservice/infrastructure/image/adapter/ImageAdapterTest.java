package pragma.roosvell.imagemicroservice.infrastructure.image.adapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pragma.roosvell.imagemicroservice.application.mapper.ImageMapper;
import pragma.roosvell.imagemicroservice.data.DataImage;
import pragma.roosvell.imagemicroservice.data.DataPerson;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;
import pragma.roosvell.imagemicroservice.infrastructure.image.entities.ImageEntity;
import pragma.roosvell.imagemicroservice.infrastructure.image.imagedao.ImageDao;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ImageAdapterTest {
    @MockBean
    ImageDao imageDao;

    @MockBean
    PersonClient personDao;

    @Autowired
    ImageMapper imageMapper;


    
    @Autowired
    ImageAdapter service; 
    
    @Test
    void createImage() throws IOException {
        when(personDao.getPersonById(1L)).thenReturn(DataPerson.createPerson1());
        when(imageDao.save(imageMapper
                .imagetoImageEntity(DataImage.createImage1().get())))
                        .thenReturn(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        Image image = service.createImage(DataImage.createMultipartfile(), 1L );
        //assertNotNull(image);
        assertEquals(personDao.getPersonById(1L).getBody().getId(), image.getPerson().getId());
        assertThrows(NotFoundExc.class, ()->{
            service.createImage(DataImage.createMultipartfile(), 2L );
        });
    }

    @Test
    void updateImage() throws IOException {
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.save(imageMapper.imagetoImageEntity(DataImage.createImage1().get())))
                .thenReturn(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        when(personDao.getPersonById(1L)).thenReturn(DataPerson.createPerson1());
        Image image1 = service.updateImage(DataImage.createMultipartfile(),"id1",1L);
        assertEquals("id1",image1.get_id());
        assertEquals(1L,image1.getPerson().getId());
        assertNotEquals(image1.getFile(),DataImage.createImage1().get().getFile());
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id1",3L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id3",1L);
        });
        verify(imageDao).save(any(ImageEntity.class));

    }

    @Test
    void getImages() {
        when(imageDao.findAll()).thenReturn(imageMapper
                .listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        List<Image> images = service.getImages();
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        verify(imageDao).findById("id1");
        verify(imageDao).findById("id2");
        verify(imageDao,times(2)).findById(any(String.class));
        verify(imageDao).findAll();
        

    }

    @Test
    void getImageById() {
        when(imageDao.findAll()).thenReturn(imageMapper
                .listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        List<Image> images = service.getImages();
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImageById("id3");
        });
        verify(imageDao, times(1)).findById("id1");
        verify(imageDao, times(1)).findById("id2");
        verify(imageDao,times(3)).findById(any(String.class));
        verify(imageDao,times(1)).findAll();
    }

    @Test
    void deleteImage() {
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper.imagetoImageEntity(DataImage.createImage1().get())));
        service.deleteImage("id1");
        assertThrows(NotFoundExc.class, ()->{
            service.deleteImage("id3");
        });
        verify(imageDao).delete(any(ImageEntity.class));
    }

    @Test
    void getImagesByPersonId() {
        when(imageDao.findByPersonId(DataPerson.createPerson1().getBody().getId()))
                .thenReturn(imageMapper.listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findAll()).thenReturn(imageMapper
                .listImageToListImageEntity(DataImage.createImages()));;
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        when(personDao.getPersonById(1L)).thenReturn(DataPerson.createPerson1());
        List<Image> images = service.getImagesByPersonId(DataPerson.createPerson1().getBody().getId());
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImagesByPersonId(3L);
        });

        verify(imageDao, times(1)).findById("id1");
        verify(imageDao, times(1)).findById("id2");
        verify(imageDao,times(2)).findById(any(String.class));
        verify(imageDao).findByPersonId(any(Long.class));


    }

    @Test
    void updatePerson() throws IOException {
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.save(imageMapper.imagetoImageEntity(DataImage.createImage1().get())))
                .thenReturn(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        when(personDao.getPersonById(1L)).thenReturn(DataPerson.createPerson1());
        Person person1 = personDao.getPersonById(1L).getBody();
        assertEquals(1L,person1.getId());
        assertNotNull(person1);
        assertThrows(NotFoundExc.class, ()->{
            service.updatePerson("id1",3L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updatePerson("id3",1L);
        });
    }
}