package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.junit.jupiter.api.Test;

import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.data.DataImage;
import pragma.roosvell.imagemicroservice.data.DataPerson;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServiceGetImagesByPersonIdTest {
    PersonClient personRepository = mock(PersonClient.class);
    ImageRepository imageRepository = mock(ImageRepository.class);
    ServiceGetImages serviceGetImages = mock(ServiceGetImages.class);
    ServiceGetImageById serviceGetImageById = mock(ServiceGetImageById.class);
    ServiceGetImagesByPersonId service = new ServiceGetImagesByPersonId(imageRepository,personRepository);


    @Test
    void getImagesByPersonId() throws NotFoundExc {
        when(imageRepository.getImagesByPersonId(DataPerson.createPerson1().getBody().getId()))
                .thenReturn(DataImage.createImages());
        when(serviceGetImages.getImages())
                .thenReturn(DataImage.createImages());
        when(serviceGetImageById.getImageById("id1")).thenReturn(DataImage.createImage1().get());
        when(serviceGetImageById.getImageById("id2")).thenReturn(DataImage.createImage2().get());
        when(personRepository.getPersonById(1L)).thenReturn(DataPerson.createPerson1());

        List<Image> images = service.getImagesByPersonId(DataPerson.createPerson1().getBody().getId());
        for (Image image:images){
            serviceGetImageById.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImagesByPersonId(3L);
        });

        verify(imageRepository, times(1)).getImagesByPersonId(1L);
        verify(imageRepository, times(1)).getImagesByPersonId(any(Long.class));
    }
}