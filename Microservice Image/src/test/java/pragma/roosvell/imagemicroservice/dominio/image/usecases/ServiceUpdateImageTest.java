package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.data.DataImage;
import pragma.roosvell.imagemicroservice.data.DataPerson;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.client.PersonClient;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;


import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServiceUpdateImageTest {
    ImageRepository imageRepository= mock(ImageRepository.class);
    PersonClient personRepository = mock(PersonClient.class);
    ServiceUpdateImage service = new ServiceUpdateImage(imageRepository,personRepository);

    @Test
    void updateImage() throws IOException {
        when(imageRepository.getImageById("id1")).thenReturn(DataImage.createImage1().get());
        when(imageRepository.updateImage(DataImage.createMultipartfile(),"id1",1L))
                .thenReturn(DataImage.createImage1().get());
        when(personRepository.getPersonById(1L)).thenReturn(DataPerson.createPerson1());
        Image imagen1 = service.updateImage(DataImage.createMultipartfile(),"id1", 1L );
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id3", 1L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id1", 3L);
        });
    }
}