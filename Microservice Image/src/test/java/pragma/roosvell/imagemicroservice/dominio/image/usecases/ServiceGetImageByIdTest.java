package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.data.DataImage;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;


import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServiceGetImageByIdTest {
    ImageRepository repository= mock(ImageRepository.class);
    ServiceGetImageById service = new ServiceGetImageById(repository);
    ServiceGetImages serviceAllImages = new ServiceGetImages(repository);

    @Test
    void getImageById() throws NotFoundExc {
        when(repository.getImages()).thenReturn(DataImage.createImages());
        when(repository.getImageById("id1")).thenReturn((DataImage.createImage1().get()));
        when(repository.getImageById("id2")).thenReturn((DataImage.createImage2().get()));
        List<Image> images = serviceAllImages.getImages();
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImageById("id3");
        });
        verify(repository, times(2)).getImageById("id1");
        verify(repository, times(2)).getImageById("id2");
        verify(repository,times(5)).getImageById(any(String.class));
        verify(repository,times(1)).getImages();
    }
}