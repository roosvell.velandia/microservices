package pragma.roosvell.imagemicroservice.dominio.image.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.imagemicroservice.application.image.ImageRepository;
import pragma.roosvell.imagemicroservice.data.DataImage;
import pragma.roosvell.imagemicroservice.infrastructure.error.NotFoundExc;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServiceDeleteImageTest {
    ImageRepository repository= mock(ImageRepository.class);
    ServiceDeleteImage service = new ServiceDeleteImage(repository);

    @Test
    void deleteImage() throws NotFoundExc {
        when(repository.getImageById("id1"))
                .thenReturn(DataImage.createImage1().get());
        service.deleteImage("id1");
        assertThrows(NotFoundExc.class, ()->{
            service.deleteImage("id3");
        });
        verify(repository, times(1)).deleteImage("id1");
        verify(repository).deleteImage(any(String.class));
    }
}