package pragma.roosvell.imagemicroservice.data;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;
import pragma.roosvell.imagemicroservice.dominio.person.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
@Data
@Getter
@Setter
public class DataPerson {


        public static final ResponseEntity<Person> createPerson1() {
           Person person1 = new Person(1L,1234L,"Roosvell","Velandia");
            ResponseEntity<Person> responseEntity1 = new ResponseEntity<Person>(person1, HttpStatus.OK);
            return responseEntity1;
        }

            public static final ResponseEntity<Person> createPerson2() {
                Person person2 = new Person(2L,4321L,"Andrea","Jimenez");
                ResponseEntity<Person> responseEntity2 = new ResponseEntity<Person>(person2, HttpStatus.OK);
                return responseEntity2;
        }

            public static final ResponseEntity<Person> createPerson3() {
                Person person3 = new Person(3L,1111L,"Rafael","Suarez");
                ResponseEntity<Person> responseEntity3 = new ResponseEntity<Person>(person3, HttpStatus.OK);
                return responseEntity3;
        }
}
