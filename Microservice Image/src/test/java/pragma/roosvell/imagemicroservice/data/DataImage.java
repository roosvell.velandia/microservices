package pragma.roosvell.imagemicroservice.data;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.imagemicroservice.dominio.image.model.Image;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Data
@Getter
@Setter
public class DataImage {
    public static byte[] file;
    public static List<Image> createImages() {
        return Arrays.asList(new Image("id1",file,DataPerson.createPerson1().getBody()),
                new Image("id2",file,DataPerson.createPerson2().getBody()));
    }

    public static final Optional<Image> createImage1(){
        return Optional.of(new Image("id1",file,DataPerson.createPerson1().getBody()));
    }

    public static final Optional<Image> createImage2(){
        return Optional.of(new Image("id2",file,DataPerson.createPerson2().getBody()));
    }

    public static final Optional<Image> createImage3(){
        return Optional.of(new Image("id3",file,DataPerson.createPerson3().getBody()));
    }

    public static final MultipartFile createMultipartfile() throws IOException {
        FileInputStream path = new FileInputStream("C:\\Users\\roosvell.velandia\\IdeaProjects\\persona-imagen-V1\\src\\test\\java\\com\\backend\\monolito\\datos\\image.png");
        MockMultipartFile multipartFile = new MockMultipartFile("file", path);

        return multipartFile;
    }
}
